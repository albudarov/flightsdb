package com.demo.flightsdb2.dto

import jakarta.validation.constraints.NotNull
import java.io.Serializable
import java.time.OffsetDateTime

/**
 * DTO for {@link com.demo.flightsdb2.domain.Flight}
 */
data class FlightDto(var id: Long? = null, @field:NotNull var number: Int? = null, @field:NotNull var airline: AirlineDto? = null, @field:NotNull var fromAirport: AirportDto? = null, @field:NotNull var toAirport: AirportDto? = null, @field:NotNull var takeoffDate: OffsetDateTime? = null, @field:NotNull var landingDate: OffsetDateTime? = null) : Serializable