package com.demo.flightsdb2.domain

import jakarta.persistence.*
import jakarta.validation.constraints.NotNull
import java.time.OffsetDateTime

@Entity
@Table(name = "airdatas_flight", indexes = [
    Index(name = "idx_flight_takeoff_date", columnList = "takeoff_date"),
    Index(name = "idx_flight_from_airport_id", columnList = "from_airport_id"),
    Index(name = "idx_flight_to_airport_id", columnList = "to_airport_id"),
    Index(name = "airdatas_flight_number_key", columnList = "number", unique = true),
    Index(name = "idx_flight_landing_date", columnList = "landing_date")
])
open class Flight {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    open var id: Long? = null

    @NotNull
    @Column(name = "number", nullable = false)
    open var number: Int? = null

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "airline_id", nullable = false)
    open var airline: Airline? = null

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "from_airport_id", nullable = false)
    open var fromAirport: Airport? = null

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "to_airport_id", nullable = false)
    open var toAirport: Airport? = null

    @NotNull
    @Column(name = "takeoff_date", nullable = false)
    open var takeoffDate: OffsetDateTime? = null

    @NotNull
    @Column(name = "landing_date", nullable = false)
    open var landingDate: OffsetDateTime? = null
}